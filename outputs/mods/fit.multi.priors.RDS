// generated with brms 2.19.0
functions {
  /* compute correlated group-level effects
   * Args:
   *   z: matrix of unscaled group-level effects
   *   SD: vector of standard deviation parameters
   *   L: cholesky factor correlation matrix
   * Returns:
   *   matrix of scaled group-level effects
   */
  matrix scale_r_cor(matrix z, vector SD, matrix L) {
    // r is stored in another dimension order than z
    return transpose(diag_pre_multiply(SD, L) * z);
  }
  /* integer sequence of values
   * Args:
   *   start: starting integer
   *   end: ending integer
   * Returns:
   *   an integer sequence from start to end
   */
  array[] int sequence(int start, int end) {
    array[end - start + 1] int seq;
    for (n in 1 : num_elements(seq)) {
      seq[n] = n + start - 1;
    }
    return seq;
  }
  // compute partial sums of the log-likelihood
  real partial_log_lik_lpmf(array[] int seq, int start, int end,
                            data vector Y_Aggsc, real Intercept_Aggsc,
                            real sigma_Aggsc, data vector Y_Boldsc,
                            real Intercept_Boldsc, real sigma_Boldsc,
                            data int nresp, data array[] vector Y,
                            matrix Lrescor, data array[] int J_1_Aggsc,
                            data array[] int J_1_Boldsc,
                            data vector Z_1_Aggsc_1,
                            data vector Z_1_Boldsc_2, vector r_1_Aggsc_1,
                            vector r_1_Boldsc_2) {
    real ptarget = 0;
    int N = end - start + 1;
    int N_Aggsc = end - start + 1;
    int N_Boldsc = end - start + 1;
    // initialize linear predictor term
    vector[N_Aggsc] mu_Aggsc = rep_vector(0.0, N_Aggsc);
    // initialize linear predictor term
    vector[N_Boldsc] mu_Boldsc = rep_vector(0.0, N_Boldsc);
    // multivariate predictor array
    array[N] vector[nresp] Mu;
    vector[nresp] sigma = transpose([sigma_Aggsc, sigma_Boldsc]);
    // cholesky factor of residual covariance matrix
    matrix[nresp, nresp] LSigma = diag_pre_multiply(sigma, Lrescor);
    mu_Aggsc += Intercept_Aggsc;
    mu_Boldsc += Intercept_Boldsc;
    for (n in 1 : N_Aggsc) {
      // add more terms to the linear predictor
      int nn = n + start - 1;
      mu_Aggsc[n] += r_1_Aggsc_1[J_1_Aggsc[nn]] * Z_1_Aggsc_1[nn];
    }
    for (n in 1 : N_Boldsc) {
      // add more terms to the linear predictor
      int nn = n + start - 1;
      mu_Boldsc[n] += r_1_Boldsc_2[J_1_Boldsc[nn]] * Z_1_Boldsc_2[nn];
    }
    // combine univariate parameters
    for (n in 1 : N) {
      int nn = n + start - 1;
      Mu[n] = transpose([mu_Aggsc[n], mu_Boldsc[n]]);
    }
    ptarget += multi_normal_cholesky_lpdf(Y[start : end] | Mu, LSigma);
    return ptarget;
  }
}
data {
  int<lower=1> N; // total number of observations
  int<lower=1> N_Aggsc; // number of observations
  vector[N_Aggsc] Y_Aggsc; // response variable
  int<lower=1> N_Boldsc; // number of observations
  vector[N_Boldsc] Y_Boldsc; // response variable
  int<lower=1> nresp; // number of responses
  int nrescor; // number of residual correlations
  int grainsize; // grainsize for threading
  // data for group-level effects of ID 1
  int<lower=1> N_1; // number of grouping levels
  int<lower=1> M_1; // number of coefficients per level
  array[N_Aggsc] int<lower=1> J_1_Aggsc; // grouping indicator per observation
  array[N_Boldsc] int<lower=1> J_1_Boldsc; // grouping indicator per observation
  // group-level predictor values
  vector[N_Aggsc] Z_1_Aggsc_1;
  vector[N_Boldsc] Z_1_Boldsc_2;
  int<lower=1> NC_1; // number of group-level correlations
  int prior_only; // should the likelihood be ignored?
}
transformed data {
  array[N] vector[nresp] Y; // response array
  array[N] int seq = sequence(1, N);
  for (n in 1 : N) {
    Y[n] = transpose([Y_Aggsc[n], Y_Boldsc[n]]);
  }
}
parameters {
  real Intercept_Aggsc; // temporary intercept for centered predictors
  real<lower=0> sigma_Aggsc; // dispersion parameter
  real Intercept_Boldsc; // temporary intercept for centered predictors
  real<lower=0> sigma_Boldsc; // dispersion parameter
  cholesky_factor_corr[nresp] Lrescor; // parameters for multivariate linear models
  vector<lower=0>[M_1] sd_1; // group-level standard deviations
  matrix[M_1, N_1] z_1; // standardized group-level effects
  cholesky_factor_corr[M_1] L_1; // cholesky factor of correlation matrix
}
transformed parameters {
  matrix[N_1, M_1] r_1; // actual group-level effects
  // using vectors speeds up indexing in loops
  vector[N_1] r_1_Aggsc_1;
  vector[N_1] r_1_Boldsc_2;
  real lprior = 0; // prior contributions to the log posterior
  // compute actual group-level effects
  r_1 = scale_r_cor(z_1, sd_1, L_1);
  r_1_Aggsc_1 = r_1[ : , 1];
  r_1_Boldsc_2 = r_1[ : , 2];
  lprior += normal_lpdf(Intercept_Aggsc | 0, 0.2);
  lprior += exponential_lpdf(sigma_Aggsc | 1);
  lprior += normal_lpdf(Intercept_Boldsc | 0, 0.2);
  lprior += exponential_lpdf(sigma_Boldsc | 1);
  lprior += lkj_corr_cholesky_lpdf(Lrescor | 1);
  lprior += exponential_lpdf(sd_1 | 1);
  lprior += lkj_corr_cholesky_lpdf(L_1 | 2);
}
model {
  // likelihood including constants
  if (!prior_only) {
    target += reduce_sum(partial_log_lik_lpmf, seq, grainsize, Y_Aggsc,
                         Intercept_Aggsc, sigma_Aggsc, Y_Boldsc,
                         Intercept_Boldsc, sigma_Boldsc, nresp, Y, Lrescor,
                         J_1_Aggsc, J_1_Boldsc, Z_1_Aggsc_1, Z_1_Boldsc_2,
                         r_1_Aggsc_1, r_1_Boldsc_2);
  }
  // priors including constants
  target += lprior;
  target += std_normal_lpdf(to_vector(z_1));
}
generated quantities {
  // actual population-level intercept
  real b_Aggsc_Intercept = Intercept_Aggsc;
  // actual population-level intercept
  real b_Boldsc_Intercept = Intercept_Boldsc;
  // residual correlations
  corr_matrix[nresp] Rescor = multiply_lower_tri_self_transpose(Lrescor);
  vector<lower=-1, upper=1>[nrescor] rescor;
  // compute group-level correlations
  corr_matrix[M_1] Cor_1 = multiply_lower_tri_self_transpose(L_1);
  vector<lower=-1, upper=1>[NC_1] cor_1;
  // extract upper diagonal of correlation matrix
  for (k in 1 : nresp) {
    for (j in 1 : (k - 1)) {
      rescor[choose(k - 1, 2) + j] = Rescor[j, k];
    }
  }
  // extract upper diagonal of correlation matrix
  for (k in 1 : M_1) {
    for (j in 1 : (k - 1)) {
      cor_1[choose(k - 1, 2) + j] = Cor_1[j, k];
    }
  }
}

