# Data simulations for among- vs. within-individual correlation examples
Simulations adapted from [Dingemanse & Dochtermann (2013)](https://doi.org/10.1111/1365-2656.12013). Code 
 adapted from Solomon Kurtz's [Statistical rethinking with brms, ggplot2, and the tidyverse: Second edition](https://bookdown.org/content/4857/adventures-in-covariance.html#varying-slopes-by-construction)

# File organisation
- `R` Scripts R
    - `01_datasims.R` Data simulations
    - `02_analysis.R` Data analysis with [brms]() package
- `outputs/` Figure and model storage
    - `fig` Figures
    - `models` Saved models

# How to use
Download or clone the repository and open the `jddabies-sims.RProject` file. Open the `01_datasims.R` to follow the data generation process and generate plots. Run the `02_analysis.R` for a more in-depth overview on the use of multivariate mixed effect models. You will need to install `cmdstanR` and a functioning C++ toolchain to make the code work. See installation instructions [here](https://mc-stan.org/cmdstanr/articles/cmdstanr.html) and [here](https://mc-stan.org/users/interfaces/cmdstan.html).
Alternatively, you may also load the fitted model directly from the `outputs/mods` folder.  

